export default {
  appTitle: 'hold circle',
  appShortTitle: process.env.VUE_APP_SHORT_TITLE,
  networkOnLine: true,
  SWRegistrationForNewContent: null,
  showAddToHomeScreenModalForApple: false,
  refreshingApp: false,
  loading: false,
  stateImageUrl: null,
  stateImageObject: {}
}
