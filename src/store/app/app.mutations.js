export default {
  setNetworkOnline: (state, value) => (state.networkOnLine = value),
  setSWRegistrationForNewContent: (state, value) =>
    (state.SWRegistrationForNewContent = value),
  setShowAddToHomeScreenModalForApple: (state, value) =>
    (state.showAddToHomeScreenModalForApple = value),
  setRefreshingApp: (state, value) => (state.refreshingApp = value),
  switchLoading: state => (state.loading = !state.loading),
  setImageUrl: (state, value) => (state.stateImageUrl = value),
  setImageObject: (state, value) => (state.stateImageObject = value),
  clearImageState: state => {
    state.imageUrl = null
    state.stateImageObject = {}
  }
}
