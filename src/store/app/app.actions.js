import { isNil } from 'lodash'
import firebase from 'firebase/app'
import UsersDB from '@/firebase/models/users-db'
import FacilitatorsProfileDB from '@/firebase/models/facilitator-profile-db'
import CirclesDB from '@/firebase/models/circle-db'

export default {
  /**
   * Closes "add to home screen" modal for apple
   */
  closeAddToHomeScreenModalForApple: async ({ commit }) => {
    localStorage.setItem('addToHomeIosPromptLastDate', Date.now())
    commit('setShowAddToHomeScreenModalForApple', false)
  },

  /**
   * Trigger service worker skipWating so the new service worker can take over.
   * This will also trigger a window refresh (see /src/misc/register-service-worker.js)
   */
  serviceWorkerSkipWaiting({ state, commit }) {
    if (isNil(state.SWRegistrationForNewContent)) return

    commit('setRefreshingApp', true)
    state.SWRegistrationForNewContent.waiting.postMessage('skipWaiting')
  },

  async uploadImage({ commit, dispatch }, data) {
    const { path, uid, type, image } = data
    if (!path || !uid || !type) return
    commit('switchLoading')
    await firebase
      .storage()
      .ref(
        `${path}/${uid}/${type}/${Math.floor(Math.random() * 10000000000000) +
          image.name}`
      )
      .put(data.image)
      .then(response => {
        response.ref.getDownloadURL().then(async url => {
          const updateDate = {
            imageUrl: url,
            id: type === 'circle' ? data.circleId : data.uid
          }
          commit('switchLoading')
          if (path === 'facilitators') {
            await FacilitatorsProfileDB.update(updateDate)
            const sendData = { id: uid, imageUrl: url }
            await commit('setImageObject', sendData)
            dispatch('circle/updateFacilitatorImage', sendData, { root: true })
            await commit('setImageUrl', url)
          }
          if (path === 'users') await new UsersDB().update(updateDate)
          if (path === 'circles') await new CirclesDB().update(updateDate)
          await commit('setImageUrl', url)
        })
      })
      .catch(err => {
        console.log(err)
        commit('switchLoading')
      })
  }
}
