import Vue from 'vue'
import Vuex from 'vuex'
import authentication from './authentication'
import app from './app'
import circle from './circle'
import facilitator from './facilitator'
import chat from './chat'
import facilitatorProfile from './facilitatorProfile'
import follow from './follow'
import conversations from './conversations'
import group from './group'
import video from './video'

Vue.use(Vuex)

/* If you don't know about Vuex, please refer to https://vuex.vuejs.org/ */

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    authentication,
    app,
    circle,
    facilitator,
    chat,
    facilitatorProfile,
    follow,
    conversations,
    group,
    video
  }
})
