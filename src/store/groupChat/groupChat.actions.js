// import router from '@/router'
import multiChatDB from '@/firebase/models/conversation-db'

export default {
  getMultiChat: async ({ commit }, id) => {
    console.log('groupchat')
    const messages = await multiChatDB.getConversation(id)
    console.log('test')
    commit('addGroupChatMessages', messages)
    return messages
  },

  async addMessageToGroup({ commit, rootState }, message) {
    const { id } = message
    const { user } = rootState.authentication
    const messageObject = {}
    messageObject.messageBody = message.messageBody
    messageObject.senderReceiver = `${user.id}-${id}`
    messageObject.receiverSender = `${id}-${user.id}`
    messageObject.groupChatId = id
    messageObject.userId = user.id
    const chat = await multiChatDB.addMessageToGroup(messageObject)
    commit('addGroupChatMessages', chat)
    return chat
  }
}
