export default {
  authErrorMessage: null,
  chatWith: {},
  groupMessages: [],
  messageSentResponse: null,
  messageSentError: null
}
