import state from './groupChat.state'
import mutations from './groupChat.mutations'
import actions from './groupChat.actions'
import getters from './groupChat.getters'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
