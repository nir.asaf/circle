export default {
  addGroupChatMessages: (state, value) => {
    state.groupMessages.push(value)
  },
  setMessageSentResponse: (state, value) => (state.messageSentResponse = value),
  setMessageSentError: (state, value) => (state.messageSentError = value)
}
