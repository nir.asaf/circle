export default {
  setAuthErrorMessages: (state, value) => (state.authErrorMessages = value),
  addSearchFacilitators: (state, value) =>
    (state.searchFacilitatorResults = value),
  setFacilitator: (state, value) => (state.facilitator = value),
  setStripeConnectCode: (state, value) => (state.code = value),
  setStripeConnectError: (state, value) => (state.stripeConnectError = value)
}
