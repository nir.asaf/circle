import router from '@/router'
import FacilitatorDB from '@/firebase/models/facilitators-db'
import FacilitatorProfileDB from '@/firebase/models/facilitator-profile-db'
import AuthService from '@/firebase/services/auth-service'

export default {
  async login({ commit }, auth) {
    const { email, password } = auth
    const authService = new AuthService()
    authService
      .login(email, password)
      .then(() => {
        router.push('/facilitator')
      })
      .catch(err => {
        console.log(err)
        commit('setAuthErrorMessages', err.message)
      })
  },

  async join({ commit }, payload) {
    const { email, password, firstName, lastName, phone } = payload
    await new AuthService()
      .createAuthUser(email, password)
      .then(async res => {
        const authUser = res.user
        const facilitator = {
          firstName: firstName || '',
          lastName: lastName || '',
          role: 'facilitator',
          email,
          uid: authUser.uid,
          phone: phone || '',
          participants: 0,
          earnings: 0
        }
        await FacilitatorDB.create(facilitator, authUser.uid)
          .then(async results => {
            await commit('authentication/setUser', results, { root: true })
            return results
          })
          .catch(err => {
            commit('setAuthErrorMessages', err.message)
          })
      })
      .catch(err => {
        commit('setAuthErrorMessages', err.message)
      })
  },

  searchFacilitators: async ({ commit }, search) => {
    const facDb = new FacilitatorProfileDB()
    facDb
      .searchByTerm(search.searchBy, search.searchTerm)
      .then(querySnapshot => {
        const searches = []
        querySnapshot.forEach(doc => {
          const data = doc.data()
          data.id = doc.id
          searches.push(data)
        })
        commit('addSearchFacilitators', searches)
      })
      .catch(err => console.log(err))
  },

  stripeConnect: async ({ commit }, code) => {
    return FacilitatorDB.setStripeConnect(code)
      .then(() => {
        commit('setStripeConnectError', null)
      })
      .catch(e => {
        console.log(e)
        commit('setStripeConnectError', e)
      })
  }
}
