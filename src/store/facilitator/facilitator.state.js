export default {
  authErrorMessages: null,
  facilitator: {},
  searchFacilitatorResults: [],
  updateUser: null,
  updateUserErrorMessage: null,
  code: null,
  stripeConnectError: null
}
