import state from './facilitator.state'
import mutations from './facilitator.mutations'
import actions from './facilitator.actions'
import getters from './facilitator.getters'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
