export default {
  allCircles: [],
  circles: [],
  circle: {},
  circleCreationPending: false,
  circleLimit: 3,
  AddCircleStatus: 'none',
  AddCircleErrorMessage: null,
  searchCircleResults: [],
  typeCirclesTerms: {},
  typeCircles: [],
  deleteCircleId: null
}

