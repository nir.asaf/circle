import CircleDB from '@/firebase/models/circle-db'
import router from '@/router'
import FacilitatorsProfile from '@/firebase/models/facilitator-profile-db'

export default {
  getAllCircles: async ({ commit }, limit) => {
    const constraints = { term: 'visibilityType', status: '==', value: true }
    const circleDb = new CircleDB()
    const circles = await circleDb.readAll([constraints], limit)
    commit('setCircles', circles)
  },

  getUserCircles: async ({ commit, rootState }) => {
    const circles = []
    const circleDb = new CircleDB()
    if (rootState.authentication.user.circles.length > 0) {
      await circleDb
        .getUserCircles(rootState.authentication.user.circles)
        .then(snapShot => {
          snapShot.forEach(data => {
            circles.push(data.data())
          })
        })
      commit('setAllCircles', circles)
    }
  },

  // eslint-disable-next-line no-empty-pattern
  createCircle: async ({ commit, rootState, dispatch }, circle) => {
    // console.log(circle)
    // if(circle.videoURL){
    //   let url = circle.videoURL
    //   url = url.split('/j/')
    //   console.log(url)
    //   circle.videoURL = url[1] ? url[1] : url
    // }
    commit('setAddCircleStatus', null)
    commit('setAddCircleErrorMessage', null)
    let { facilitatorProfile } = rootState.facilitatorProfile
    const userId = rootState.authentication.user.id
    if (!facilitatorProfile) {
      facilitatorProfile = await FacilitatorsProfile.read(userId)
    }
    if (!facilitatorProfile) {
      dispatch('facilitatorProfile/getFacilitatorProfile', userId, {
        root: true
      })
    }
    const circleDb = new CircleDB()
    return circleDb
      .createCircle(circle, facilitatorProfile)
      .then(res => {
        res.users = []
        commit('circle/addCircle', res, { root: true })
        commit('setAddCircleStatus', 'success')
        router.push('/facilitator')
      })
      .catch(err => {
        console.log(err)
        commit('setAddCircleStatus', 'error')
        commit(
          'setAddCircleErrorMessage',
          err.message ? err.message : JSON.stringify(err)
        )
      })
  },

  triggerSetAddCircleStatus: ({ commit }, status) => {
    commit('setAddCircleStatus', status)
  },

  getMyCircles({ commit, rootState }) {
    commit('addToCircleList', [])
    const { user } = rootState.authentication
    if (user.circles) {
      FacilitatorsProfile.getMyCircles(user.id)
        .then(querySnapshot => {
          const circles = []
          querySnapshot.forEach(doc => {
            const data = doc.data()
            data.id = doc.id
            circles.push(data)
          })
          commit('addToCircleList', circles)
        })
        .catch(error => {
          console.log(error)
        })
    }
  },

  async getCircle({ commit }, id) {
    const circleDb = new CircleDB()
    const circle = await circleDb.read(id)
    if (circle) {
      commit('setCircle', circle)
      return circle
    }
    return router.push('/')
  },

  async updateCircle({ commit }, circle) {
    const circleDb = new CircleDB()

    const result = await circleDb.update(circle)
    if (result) {
      commit('setAddCircleStatus', 'success')
      await router.push('/facilitator/groups')
    } else {
      commit('setAddCircleStatus', 'error')
    }
  },

  async addCircleToUser({ commit, rootState }, id) {
    const { user } = rootState.authentication
    const circleDb = new CircleDB()
    const circle = await circleDb.read(id)
    if (!circle) {
      await router.push('/')
    }
    const circleRes = await circleDb.addCircleToUser(circle, user.id)
    if (circleRes) {
      commit('authentication/addCircleToUser', id, { root: true })
      commit('circle/addCircle', circle, { root: true })
    }
    return circleRes
  },

  searchCircles: async ({ commit }, search) => {
    const circleDb = new CircleDB()
    circleDb
      .searchByTerm(search.searchBy, search.searchTerm)
      .then(querySnapshot => {
        const searches = []
        querySnapshot.forEach(doc => {
          const data = doc.data()
          data.id = doc.id
          searches.push(data)
        })
        commit('addSearchCircles', searches)
      })
      .catch(err => console.log(err))
  },

  checkBookedCircle: async ({ rootState }, circleId) => {
    const { user } = rootState.authentication
    const circleDb = new CircleDB()
    const circle = await circleDb.checkBookedCircle(user.id, circleId)
    if (!circle) {
      return null
    }
    let booked = false
    user.circles.forEach(userCircle => {
      if (userCircle === circle.id) {
        booked = true
      }
    })
    return booked
  },

  updateFacilitatorImage: async ({ rootState }) => {
    const object = rootState.app.stateImageObject
    const circleDb = new CircleDB()
    return circleDb.updateFacilitatorImage(object)
  },

  getTypeCircles: async ({ commit, rootState }) => {
    const { typeCirclesTerms } = rootState.circle
    const { typeCircles } = rootState.circle
    if (
      typeCircles &&
      typeCircles.length > 0 &&
      typeCircles[0].circleType === typeCirclesTerms.circleType
    ) {
      return typeCircles
    }
    if (typeCirclesTerms) {
      const circleDb = new CircleDB()
      const circles = []
      await circleDb.getTypeCircles(typeCirclesTerms).then(doc => {
        doc.forEach(data => {
          if (data.id !== typeCirclesTerms.exceptId) {
            circles.push(data.data())
          }
        })
      })
      commit('addTypeCircles', circles)
      return circles
    }
    return []
  },

  deleteCircle: async ({ rootState }) => {
    const id = rootState.circle.deleteCircleId
    const circleDb = new CircleDB()
    return circleDb.deleteCircle(id)
  }
}
