import state from './circle.state'
import mutations from './circle.mutations'
import actions from './circle.actions'
import getters from './circle.getters'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
