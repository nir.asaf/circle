export default {
  setAddCircleStatus: (state, value) => (state.AddCircleStatus = value),
  setAddCircleErrorMessage: (state, value) =>
    (state.AddCircleErrorMessage = value),
  addToCircleList: (state, value) => (state.circles = value),
  setAllCircles: (state, value) => (state.allCircles = value),
  setCircle: (state, value) => (state.circle = value),
  setCircles: (state, value) => (state.circles = value),
  addSearchCircles: (state, value) => (state.searchCircleResults = value),
  addCircle: (state, value) => state.circles.push(value),
  addTypeCircles: (state, value) => (state.typeCircles = value),
  addTypeCirclesTerms: (state, value) => (state.typeCirclesTerms = value),
  addDeleteCircleId: (state, value) => (state.deleteCircleId = value),
  deleteFromCircles: (state, value) => {
    state.circles.forEach((circle, index) => {
      if (circle.id === value) {
        state.circles.splice(index, 1)
      }
    })
  }
}
