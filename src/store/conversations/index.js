import state from './conversations.state'
import mutations from './conversations.mutations'
import actions from './conversations.actions'
import getters from './conversations.getters'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
