export default {
  authErrorMessage: null,
  chatWith: {},
  groupMessages: [],
  groupChatInfo: {},
  messageSentResponse: null,
  messageSentError: null,
  groupChatId: null,
  message: null,
}
