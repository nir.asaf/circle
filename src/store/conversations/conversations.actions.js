// import router from '@/router'
import conversationDb from '@/firebase/models/conversation-db'

export default {
  getConversations: async ({ rootState }) => {
    const { id } = rootState.authentication.user
    return conversationDb.getConversations(id)
  },

  addMessageToGroup: async ({ rootState }) => {
    const { message } = rootState.conversations
    if (message.groupChatId) {
      return conversationDb.addMessageToGroup(message)
    }
    return 0
  },

  getConversation: async ({ rootState, commit }) => {
    const { groupChatId } = rootState.conversations
    if (groupChatId) {
      try {
        const res = await conversationDb.getConversation(groupChatId)
        res.onSnapshot(snapshot => {
          if (!snapshot.empty) {
            snapshot.docChanges().forEach(change => {
              const { groupMessages } = rootState.conversations
              const lastIndex =
                groupMessages.length > 0
                  ? groupMessages[groupMessages.length - 1].newIndex
                  : 0

              if (change.type === 'added' && (groupMessages.length === 0 || change.newIndex !== lastIndex)) {
                const msg = change.doc.data()
                msg.newIndex = change.newIndex
                commit('conversations/addGroupChatMessages', msg, {
                  root: true
                })
              }
            })
          }
        })
      } catch (e) {
        console.log(e)
      }
    }
    return 1
  },

  getConversationData: async ({ commit, rootState }) => {
    const { groupChatId } = rootState.conversations
    const res = await conversationDb.getConversationData(groupChatId)
    if (res.exists) {
      commit('setGroupChatInfo', res.data())
    }
  }
}
