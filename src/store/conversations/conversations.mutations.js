export default {
  addGroupChatId: (state, value) => (state.groupChatId = value),
  addMessage: (state, value) => {
    state.message = value
  },
  addGroupChatMessages: (state, value) => {
    state.groupMessages.push(value)
  },
  setGroupChatInfo: (state, value) => {
    state.groupChatInfo = value
  },
  clearConversations: state => {
    state.groupChatId = null
    state.groupChatInfo = {}
    state.groupMessages = []
  }
}
