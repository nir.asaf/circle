import state from './chat.state'
import mutations from './chat.mutations'
import actions from './chat.actions'
import getters from './chat.getters'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
