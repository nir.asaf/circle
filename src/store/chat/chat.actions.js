import router from '@/router'
import ChatDB from '@/firebase/models/chat-db'
import FacilitatorsDb from '@/firebase/models/facilitators-db'
import UsersDB from '@/firebase/models/users-db'

export default {
  getChatUser: async ({ commit, rootState }, id) => {
    console.log('getChatUser')
    const { user } = rootState.authentication
    const senderReceiver = `${user.id}-${id}`
    const receiverSender = `${id}-${user.id}`
    const chatDb = new ChatDB()
    if (!(await FacilitatorsDb.read(id))) {
      console.log('redirect')
      await router.push('/')
    }
    const messages = await chatDb.getChatWith(senderReceiver, receiverSender)
    commit('addChatMessages', messages)
  },
  getChatFacilitator: async ({ commit, rootState }, id) => {
    console.log('getChatFacilitator')
    const { user } = rootState.authentication
    const senderReceiver = `${user.id}-${id}`
    const receiverSender = `${id}-${user.id}`
    const chatDb = new ChatDB()
    const usersDb = new UsersDB()
    if (!(await usersDb.read(id))) {
      await router.push('/')
    }
    const messages = await chatDb.getChatWith(senderReceiver, receiverSender)
    commit('addChatMessages', messages)
  },

  async addMessageUser({ rootState }, message) {
    const { id } = message
    const { user } = rootState.authentication
    const messageObject = {}
    messageObject.messageBody = message.messageBody
    messageObject.senderReceiver = `${user.id}-${id}`
    messageObject.receiverSender = `${id}-${user.id}`
    messageObject.facilitatorId = id
    messageObject.userId = user.id
    const chatDb = new ChatDB()
    if (!(await FacilitatorsDb.read(id))) {
      await router.push('/')
    }
    return chatDb.addMessageToFacilitator(messageObject)
  },

  async addMessageFacilitator({ rootState }, message) {
    const { id } = message
    const { user } = rootState.authentication
    const messageObject = {}
    messageObject.messageBody = message.messageBody
    messageObject.senderReceiver = `${user.id}-${id}`
    messageObject.receiverSender = `${id}-${user.id}`
    messageObject.facilitatorId = id
    messageObject.userId = user.id
    const chatDb = new ChatDB()
    const usersDb = new UsersDB()
    if (!(await usersDb.read(id))) {
      await router.push('/')
    }
    return chatDb.addMessageToFacilitator(messageObject)
  }
}
