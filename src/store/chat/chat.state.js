export default {
  authErrorMessage: null,
  chatWith: {},
  messages: [],
  messageSentResponse: null,
  messageSentError: null
}
