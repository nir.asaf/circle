import state from './video.state'
import mutations from './video.mutations'
import actions from './video.actions'
import getters from './video.getters'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
