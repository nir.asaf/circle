// import router from '@/router'
import groupDb from '@/firebase/models/group-db'

export default {
  get: async({ commit }, id) => {
    const group = await groupDb.read(id)
    if (group) commit('setGroup', group)
  },
  sendGroupInvite:  async({ commit }, data) => {
    await groupDb.sendGroupInvite(data)
    commit('addGroupMember')
  }
}
