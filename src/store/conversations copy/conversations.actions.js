// import router from '@/router'
import conversationDb from '@/firebase/models/conversation-db'

export default {
  getConversations: async ({ rootState }) => {
    const { id } = rootState.authentication.user
    return conversationDb.getChatGroups(id)
  },
  getConversation: ({ rootState }) => {
    return conversationDb.getMultiChat(rootState.conversations.groupChatId)
  },

  addMessage: ({ rootState }) => {
    return conversationDb.addMessageToGroup(rootState.conversations.message)
  }
}
