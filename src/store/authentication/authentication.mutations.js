export default {
  setUser: (state, value) => state.user = value,
  setUpdateUser: (state, value) => (state.updateUser = value),
  setUserUpdated: (state, value) => state.userUpdated = value,
  setAuthErrorMessages: (state, value) => (state.authErrorMessages = value),
  clearAuthErrors: state => (state.authErrorMessages = null),
  addCircleToUser: (state, value) => state.user.circles.push(value),
  addCardToUser: (state, value) => state.user.card = value
}
