import router from '@/router'
import UsersDB from '@/firebase/models/users-db'
import FacilitatorsDB from '@/firebase/models/facilitators-db'
import FacilitatorProfileDB from '@/firebase/models/facilitator-profile-db'
import AuthService from '@/firebase/services/auth-service'
import { Role } from '@/constants'

export default {
  async login({ commit }, auth) {
    const { email, password } = auth
    const authService = new AuthService()
    return authService
      .login(email, password)
      .then(() => {
        router.push('/dashboard')
      })
      .catch(err => {
        console.log(err)
        commit('setAuthErrorMessages', err.message)
      })
  },

  async logout({ commit }) {
    const authService = new AuthService()
    authService.logout().then(() => {
      commit('setUser', null)
      commit('setUpdateUser', null)
      commit('circle/setCircles', [], { root: true })
      commit('circle/setAllCircles', [], { root: true })
      const { meta } = router.app.$route

      if (meta && meta.authRequired) {
        router.push('/')
      }
    })
  },

  async signUp({ commit }, payload) {
    const { email, password, firstName, lastName, phone } = payload
    const authService = new AuthService()
    return authService
      .createAuthUser(email, password)
      .then(async res => {
        const authUser = res.user
        const userData = {
          firstName,
          lastName,
          role: 'user',
          email,
          uid: authUser.uid,
          phone: phone || '',
          circles: []
        }
        const userDB = new UsersDB()
        userDB
          .create(userData, authUser.uid)
          .then(results => {
            commit('setUser', results)
            commit('setUpdateUser', results)
            router.push('/dashboard')
            return results
          })
          .catch(err => {
            commit('setAuthErrorMessages', err)
          })
      })
      .catch(err => {
        commit('setAuthErrorMessages', err.message)
      })
  },

  async getCurrentUser({ state, commit, dispatch }, firebaseUser) {
    let user = null
    let facilitator = false

    if (!state.user) {
      await firebaseUser.getIdToken(true)
      const idTokenResult = await firebaseUser.getIdTokenResult()
      facilitator = idTokenResult.claims.facilitator
    }

    if ((state.user && state.user.role === Role.facilitator) || facilitator) {
      user = await FacilitatorsDB.read(firebaseUser.uid)
      if (!user.profileCreated) {
        const profile = await FacilitatorProfileDB.read(user.id)
        if (!profile) {
          dispatch('facilitatorProfile/create', user, { root: true })
        } else {
          commit('facilitatorProfile/setProfile', profile, { root: true })
        }
      } else {
        const profile = await FacilitatorProfileDB.get(user.id)
        commit('facilitatorProfile/setProfile', profile, { root: true })
      }
    } else {
      user = await new UsersDB().read(firebaseUser.uid)
    }

    commit('setUser', user)
    commit('setUpdateUser', user)
  },

  async updateUser({ commit }, user) {
    if (user) {
      const usersDb = new UsersDB()
      const updatedUser = await usersDb.update(user)
      if (updatedUser) {
        commit('setUser', user)
        return false
      }
      return true
    }
    return true
  },

  async updateFacilitator({ commit }, facilitator) {
    if (facilitator) {
      const updatedFacilitator = await FacilitatorsDB.update(facilitator)
      if (updatedFacilitator) {
        commit('setUser', facilitator)
        return false
      }
      return true
    }
    return true
  },

  clearAuthErrors({ commit }) {
    commit('clearAuthErrors')
  },

  setUpdateUser({ commit }, user) {
    commit('setUpdateUser', user)
  },

  addUserStripeToken: async ({ commit }, data) => {
    const res = await new UsersDB().addUserStripeToken(data)
    if (res) {
      await commit('addCardToUser', data)
    }
    return res
  },

  forgetPassword: async ({ commit }, email) => {
    const authService = new AuthService()
    await authService.forgetPassword(email)
    commit('clearAuthErrors')
    return { msg: `Email instruction has been sent to ${email}` }
  }
}
