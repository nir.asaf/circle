export default {
  setAuthErrorMessage: (state, value) => (state.authErrorMessage = value),
  addSearchFacilitators: (state, value) =>
    (state.searchFacilitatorResults = value),
  setFacilitator: (state, value) => (state.facilitator = value),
  setUpdateUserTrue: (state, value) => (state.updateUser = value),
  setUpdateUserErrorMessage: (state, value) =>
    (state.updateUserErrorMessage = value)
}
