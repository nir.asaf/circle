import state from './follow.state'
import mutations from './follow.mutations'
import actions from './follow.actions'
import getters from './follow.getters'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
