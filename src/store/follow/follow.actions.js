import router from '@/router'
import FacilitatorDB from '@/firebase/models/facilitators-db'
import FollowDB from '@/firebase/models/follow-db'

export default {
  followFacilitator: async ({ commit, rootState }, facilitatorId) => {
    const userId = rootState.authentication.user.id
    const facilitator = FacilitatorDB.read(facilitatorId)
    if (!facilitator) {
      await router.push('/')
    }
    const followDb = new FollowDB()
    const follow = await followDb.followFacilitator(facilitatorId, userId)
    if (follow) {
      commit(
        'facilitatorProfile/updateFacilitatorProfile',
        { state: 'follow', value: !follow.empty },
        { root: true }
      )
    }
    return follow
  },

  unFollowFacilitator: async ({ commit, rootState }, facilitatorId) => {
    const userId = rootState.authentication.user.id
    const facilitator = FacilitatorDB.read(facilitatorId)
    if (!facilitator) {
      await router.push('/')
    }
    const followDb = new FollowDB()
    const unFollow = await followDb.unFollowFacilitator(facilitatorId, userId)
    if (unFollow) {
      commit(
        'facilitatorProfile/updateFacilitatorProfile',
        { state: 'follow', value: !unFollow.empty },
        { root: true }
      )
    }
    return unFollow
  },

  checkFollow: async ({ rootState }, facilitatorId) => {
    const userId = rootState.authentication.user.id
    const followDb = new FollowDB()
    return followDb.checkFollow(facilitatorId, userId)
  }
}
