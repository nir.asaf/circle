import state from './facilitatorProfile.state'
import mutations from './facilitatorProfile.mutations'
import actions from './facilitatorProfile.actions'
import getters from './facilitatorProfile.getters'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
