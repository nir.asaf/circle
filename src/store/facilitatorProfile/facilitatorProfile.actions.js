import router from '@/router'
import FacilitatorsProfile from '@/firebase/models/facilitator-profile-db'

export default {
  create: async ({ commit }, payload) => {
    const res = await FacilitatorsProfile.create(payload, payload.uid)
    if (res) commit('setProfile', res)
  },

  update: async ({ commit, rootState }, payload) => {
    const updatedImage = rootState.app.stateImageUrl
    if (updatedImage) {
      payload.imageUrl = updatedImage
    }
    if (!payload.experiences) {
      payload.experiences = []
    }
    if (!payload.circleTypes) {
      payload.circleTypes = []
    }
    if (!payload.id) {
      payload.id = rootState.authentication.user.id
    }
    const update = await FacilitatorsProfile.update(payload)
    commit('setProfile', payload)
    return update
  },

  getFacilitatorProfile: async ({ dispatch, commit, rootState }, id) => {
    const profile = await FacilitatorsProfile.get(id)
    const loggedInUser = rootState.authentication.user
    let follow = false
    if (loggedInUser) {
      follow = await dispatch('follow/checkFollow', id, { root: true })
    }
    commit('setProfile', null)
    if (profile) {
      profile.follow = follow
      commit('setProfile', profile)
      return true
    }
    await router.push('/')
    return null
  },

  searchFacilitators: async ({ commit }, search) => {
    FacilitatorsProfile.searchByTerm(
      search.experience,
      search.licensed,
      search.searchTerm
    )
      .then(querySnapshot => {
        const searches = []
        querySnapshot.forEach(doc => {
          commit('setSearchFacilitatorsProfilesEmpty', false)
          if (
            search.experience &&
            search.experience === doc.data().experience &&
            search.licensed === doc.data().licensed
          ) {
            const data = doc.data()
            data.id = doc.id
            searches.push(data)
          } else if (
            search.licensed === doc.data().licensed &&
            !search.experience
          ) {
            const data = doc.data()
            data.id = doc.id
            searches.push(data)
          }
        })
        if (searches.length === 0) {
          commit('setSearchFacilitatorsProfilesEmpty', true)
        }
        commit('addSearchFacilitatorsProfile', searches)
      })
      .catch(err => console.log(err))
  },

  showAllFacilitators: async ({ commit }) => {
    const facilitators = await FacilitatorsProfile.getAllFacilitators()
    commit('addSearchFacilitatorsProfile', facilitators)
  },

  getMyCircles: async ({ commit, rootState }) => {
    commit('circle/addToCircleList', [], { root: true })
    const { user } = rootState.authentication
    if (user.circles) {
      FacilitatorsProfile.getMyCircles(user.id)
        .then(querySnapshot => {
          const circles = []
          querySnapshot.forEach(doc => {
            if (doc.id !== 'undefined') {
              const data = doc.data()
              data.id = doc.id
              circles.push(data)
            }
          })
          commit('circle/addToCircleList', circles, { root: true })
        })
        .catch(error => {
          console.log(error)
        })
    }
  }
}
