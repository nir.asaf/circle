export default {
  setProfile: (state, value) => (state.facilitatorProfile = value),
  addSearchFacilitatorsProfile: (state, value) =>
    (state.searchFacilitatorsProfiles = value),
  setSearchFacilitatorsProfilesEmpty: (state, value) =>
    (state.searchFacilitatorsProfilesEmpty = value),
  updateFacilitatorProfile: (state, value) => {
    state.facilitatorProfile[value.state] = value.value
  }
}
