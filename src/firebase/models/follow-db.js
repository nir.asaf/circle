import GenericDB from '../generic-db'
import firestore from '../async-firestore'

export default class UsersDB extends GenericDB {
  constructor() {
    super('follow')
  }

  // Here you can extend UserDB with custom methods
  async followFacilitator(facilitatorId, userId) {
    const doesFollow = await this.doesFollow(facilitatorId, userId)
    if (doesFollow.empty) {
      await (await firestore()).collection(this.collectionPath).add({
        type: 'followFacilitator',
        followSenderId: userId,
        followReceiverId: facilitatorId,
        createdAt: new Date()
      })
      return this.doesFollow(facilitatorId, userId)
    }
    return null
  }

  async unFollowFacilitator(facilitatorId, userId) {
    const doesFollow = await this.doesFollow(facilitatorId, userId)
    if (!doesFollow.empty) {
      await (await firestore())
        .collection(this.collectionPath)
        .doc(doesFollow.id)
        .delete()
      return this.doesFollow(facilitatorId, userId)
    }
    return null
  }

  async checkFollow(facilitatorId, userId) {
    const doesFollow = await this.doesFollow(facilitatorId, userId)
    return !doesFollow.empty
  }

  async doesFollow(facilitatorId, userId) {
    let doesFollow = {}
    // .where('followReceiverId', '==', facilitatorId)
    //   .limit(1)

    await (await firestore())
      .collection(this.collectionPath)
      .where('followSenderId', '==', userId)
      .where('followReceiverId', '==', facilitatorId)
      .limit(1)
      .get()
      .then(res => {
        doesFollow = res
        res.forEach(doc => {
          doesFollow.id = doc.id
        })
      })
      .catch(err => {
        console.log(err)
      })

    return doesFollow
  }
}
