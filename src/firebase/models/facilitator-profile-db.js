import firestore from '@/firebase/async-firestore'
// import firebase from 'firebase'
import GenericDB from '../generic-db'

class FacilitatorsProfileDB extends GenericDB {
  constructor() {
    super('facilitatorsProfile')
  }

  // Here you can extend UserDB with custom methods
  async searchByTerm(experience, licensed, searchTerm) {
    return (await firestore())
      .collection(this.collectionPath)
      .where('firstName', '>=', searchTerm)
      .get()
  }

  async getAllFacilitators() {
    return (await firestore())
      .collection(this.collectionPath)
      .get()
      .then(snapshot => {
        const facilitators = []
        snapshot.forEach(doc => {
          facilitators.push(doc.data())
        })
        return facilitators
      })
  }

  async get(facilitatorId) {
    const facilitatorProfile = await this.read(facilitatorId)

    const circles = []
    const circlesRef = await (await firestore())
      .collection(this.collectionPath)
      .doc(facilitatorId)
      .collection('circles')
      .get()

    circlesRef.forEach(circle => {
      circles.push(circle.data())
    })
    return { ...facilitatorProfile, circles }
  }

  async getMyCircles(id) {
    return (await firestore())
      .collection(this.collectionPath)
      .doc(id)
      .collection('circles')
      .get()
  }
}

const FacilitatorsProfile = new FacilitatorsProfileDB()
export default FacilitatorsProfile
