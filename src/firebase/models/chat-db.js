import firestore from '@/firebase/async-firestore'
import GenericDB from '../generic-db'

export default class ChatDB extends GenericDB {
  constructor() {
    super(`chat`)
  }

  // Here you can extend UserProductsDB with custom methods
  async getChatWith(senderReceiver, receiverSender) {
    const messages = []
    await (await firestore())
      .collection(this.collectionPath)
      .where('senderIdReceiverId', 'in', [senderReceiver, receiverSender])
      .orderBy('createdAt')
      .onSnapshot(snapshot => {
        if (!snapshot.empty) {
          snapshot.docChanges().forEach(change => {
            if (change.type === 'added') {
              messages.push(change.doc.data())
            }
          })
        }
      })
    return messages
  }

  async addMessageToFacilitator(messageObject) {
    return (await firestore()).collection(this.collectionPath).add({
      senderIdReceiverId: messageObject.senderReceiver,
      body: messageObject.messageBody,
      sender: messageObject.userId,
      receiver: messageObject.facilitatorId,
      createdAt: new Date()
    })
  }
}
