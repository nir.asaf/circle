import firestore from '@/firebase/async-firestore'
import GenericDB from '../generic-db'

class ConversationsDb extends GenericDB {
  constructor() {
    super(`conversations`)
  }

  async addMessageToGroup(message) {
    return (await firestore())
      .collection(this.collectionPath)
      .doc(message.groupChatId)
      .collection('messages')
      .add({
        senderIdReceiverId: message.senderReceiver,
        body: message.messageBody,
        senderId: message.userId,
        receiverId: message.groupChatId,
        createdAt: new Date()
      })
  }

  async getConversations(facilitatorId) {
    return (await firestore())
      .collection(this.collectionPath)
      .where('facilitatorId', '==', facilitatorId)
      .get()
      .then(snapshot => {
        const chatGroups = []
        snapshot.forEach(doc => {
          chatGroups.push(doc.data())
        })
        return chatGroups
      })
      .catch(err => {
        console.log(err)
      })
  }

  async getConversation(conversationId) {
    return (await firestore())
      .collection(this.collectionPath)
      .doc(conversationId)
      .collection('messages')
      .orderBy('createdAt')
    // .startAfter(
    //   'December 4, 2020 at 10:23:52 AM UTC+1'
    // )
    // .limit(5)
    // .startAfter(1607073832)
    // .limit(5)

    // nanoseconds 942000000
    // seconds 1607073832
  }

  async getConversationData(conversationId) {
    return (await firestore())
      .collection(this.collectionPath)
      .doc(conversationId)
      .get()
  }
}

const conversationDb = new ConversationsDb()
export default conversationDb
