// import firestore from '@/firebase/async-firestore'
import firebase from 'firebase'
import GenericDB from '../generic-db'

class GroupDB extends GenericDB {
  constructor() {
    super(`groups`)
  }
  
  sendGroupInvite =  async (data) => {
    const bookCircle = firebase.functions().httpsCallable('bookCircle')
    return bookCircle(data)
  }
}

const groupDB = new GroupDB()
export default groupDB