import firebase from 'firebase'
import GenericDB from '../generic-db'

export default class UsersDB extends GenericDB {
  constructor() {
    super('users')
  }

  async addUserStripeToken(data) {
    const createUserStripeToken = firebase
      .functions()
      .httpsCallable('createUserStripeToken')
    return createUserStripeToken({
      card: data,
      path: this.collectionPath
    })
  }
}
