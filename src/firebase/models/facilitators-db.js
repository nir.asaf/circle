import firebase from 'firebase'
import GenericDB from '../generic-db'

class FacilitatorsDB extends GenericDB {
  constructor() {
    super('facilitators')
  }

  // Here you can extend UserDB with custom methods
  setStripeConnect = code => {
    const setStripeConnect = firebase
      .functions()
      .httpsCallable('setStripeConnect')
    return setStripeConnect(code)
  }
}

const Facilitators = new FacilitatorsDB()
export default Facilitators
