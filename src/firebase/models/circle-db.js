import firebase from 'firebase'
import firestore from '@/firebase/async-firestore'
import GenericDB from '../generic-db'

export default class CircleDB extends GenericDB {
  constructor() {
    super(`circles`)
  }

  // Here you can extend UserProductsDB with custom methods
  async addCircleToUser(circle, userId) {
    const bookCircle = firebase.functions().httpsCallable('bookCircle')
    return bookCircle({
      circleId: circle.id,
      id: userId,
      path: this.collectionPath
    })
      .then(res => {
        return res
      })
      .catch(err => {
        return err
      })
  }
  // "circleId": "OZmR8E9tWeKUcN7Rq7P1",
  // "id": "XuKQUY5oqrdTsF0xkqbLV174Oio1",
  // "path": "users"

  async getUserCircles(circles) {
    const firestoreAwait = await firestore()
    return firestoreAwait
      .collection(this.collectionPath)
      .where('id', 'in', circles)
      .get()
  }

  async getMyCircles(id) {
    return (await firestore())
      .collection(this.collectionPath)
      .where('facilitatorId', '==', id)
      .get()
  }

  async createCircle(circle, facilitatorProfile) {
    const firestoreAwait = await firestore()
    const circleObject = {
      ...circle,
      facilitatorId: facilitatorProfile.id,
      facilitator: {
        firstName: facilitatorProfile.firstName,
        lastName: facilitatorProfile.lastName,
        about: facilitatorProfile.about ?? '',
        imageUrl: facilitatorProfile.imageUrl
          ? facilitatorProfile.imageUrl
          : 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        title: facilitatorProfile.title ?? ''
      },
      users: []
    }
    const createRes = await firestoreAwait
      .collection(this.collectionPath)
      .add(circleObject)
      .catch(err => {
        console.log(err)
      })
    circleObject.id = createRes.id
    // await firestoreAwait
    //   .collection('facilitators')
    //   .doc(facilitatorProfile.id)
    //   .update({
    //     circles: firebase.firestore.FieldValue.arrayUnion(createRes.id)
    //   }).catch(err => {
    //     console.log(err)
    //   })
    return circleObject
  }

  async checkBookedCircle(userId, circleId) {
    let circle = null
    await (await firestore())
      .collection(this.collectionPath)
      .doc(circleId)
      .get()
      .then(snapshot => {
        circle = snapshot.data()
      })
    return circle
  }

  async updateFacilitatorImage(data) {
    return (await firestore())
      .collection(this.collectionPath)
      .where('facilitatorId', '==', data.id)
      .get()
      .then(res => {
        res.forEach(async doc => {
          await (await firestore())
            .collection(this.collectionPath)
            .doc(doc.id)
            .update({
              'facilitator.imageUrl': data.imageUrl
            })
        })
      })
  }

  async getTypeCircles(typeCirclesTerms) {
    return (await firestore())
      .collection(this.collectionPath)
      .where('circleType', '==', typeCirclesTerms.circleType)
      .get()
      .catch(err => {
        console.log(err)
      })
  }

  async deleteCircle(circleId) {
    const deleteCircle = firebase.functions().httpsCallable('deleteCircle')
    return deleteCircle({ circleId, path: this.collectionPath }).catch(err => {
      return err
    })
  }
}
