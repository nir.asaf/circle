import firebase from 'firebase/app'
import { isNil } from 'lodash'

import store from '@/store'

firebase.auth().onAuthStateChanged(async firebaseUser => {
  if (!isNil(firebaseUser)) {
    store.dispatch(`authentication/getCurrentUser`, firebaseUser)
  } else {
    store.dispatch(`authentication/logout`)
  }
})
