import firebase from 'firebase/app'
import 'firebase/auth'

// The configuration below is not sensitive data. You can serenely add your config here
const config = {
  apiKey: 'AIzaSyDsSumv1WYFMaqR9MNTOzp0tug-J-ngZ64',
  authDomain: 'circle-1866c.firebaseapp.com',
  databaseURL: 'https://circle-1866c.firebaseio.com',
  projectId: 'circle-1866c',
  storageBucket: 'circle-1866c.appspot.com',
  messagingSenderId: '556549736986',
  appId: '1:556549736986:web:c36a6d726e2ca8b7b45fa3'
}

firebase.initializeApp(config)
