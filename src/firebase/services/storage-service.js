import firebase from 'firebase/app'
import { isNil } from 'lodash'

let asyncStorage = null

// Lazy load firesbase Storage with async import is important for performance

export default () => {
  if (isNil(asyncStorage)) {
    asyncStorage = import(
      /* webpackChunkName: "chunk-firestore" */ 'firebase/storage'
    ).then(() => {
      // firebase.firestore().enablePersistence({ synchronizeTabs: true })
      return firebase.storage()
    })
  }
  return asyncStorage
}
