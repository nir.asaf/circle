import firebase from 'firebase/app'

export default class AuthService {
  constructor() {
    this.auth = firebase.auth()
  }

  async login(email, password) {
    await this.auth.signInWithEmailAndPassword(email, password)
  }

  createAuthUser(email, password) {
    return this.auth.createUserWithEmailAndPassword(email, password)
  }

  async logout() {
    return this.auth.signOut()
  }

  async forgetPassword(email) {
    return this.auth.sendPasswordResetEmail(email)
  }

  static getFirebaseUser = async () => {
    await this.auth.currentUser
  }
}
