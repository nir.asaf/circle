import { Role } from '@/constants'

export default [
  {
    path: '/facilitator',
    name: 'facilitator-dashboard',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/components/Facilitator/FacilitatorDashboard.vue'
      ),
    children: [
      {
        path: '',
        name: 'dashboard',
        component: () =>
          import(
            /* webpackChunkName: "client-chunk-login" */ '@/components/Facilitator/Dashboard/index.vue'
          ),
        meta: { authRequired: true, authorize: Role.FACILITATOR }
      },
      {
        path: 'conversations',
        name: 'facilitator-conversations',
        component: () =>
          import(
            /* webpackChunkName: "client-chunk-login" */ '@/components/Facilitator/Conversations/index.vue'
          ),
        meta: { authRequired: true, authorize: Role.FACILITATOR }
      },
      {
        path: 'conversation/:id',
        name: 'facilitator-conversation',
        component: () =>
          import(
            /* webpackChunkName: "client-chunk-login" */ '@/components/Shared/Chat/Facilitator/mobileIndex.vue'
          ),
        meta: { authRequired: true, authorize: Role.FACILITATOR }
      },
      {
        path: 'account',
        name: 'facilitator-account',
        component: () =>
          import(
            /* webpackChunkName: "client-chunk-login" */ '@/components/Facilitator/Account/index.vue'
          ),
        meta: { authRequired: true, authorize: Role.FACILITATOR }
      },
      {
        path: 'groups',
        name: 'groups',
        component: () =>
          import(
            /* webpackChunkName: "client-chunk-login" */ '@/components/Facilitator/Circles/Groups.vue'
          ),
        meta: { authRequired: true, authorize: Role.FACILITATOR }
      },
      {
        path: 'groups/:id',
        name: 'group',
        component: () =>
          import(
            /* webpackChunkName: "client-chunk-login" */ '@/components/Facilitator/Circles/Group.vue'
          ),
        meta: { authRequired: true, authorize: Role.FACILITATOR }
      },
      {
        path: 'new-circle',
        name: 'NewCircle',
        component: () =>
          import(
            /* webpackChunkName: "client-chunk-login" */ '@/components/Facilitator/Circles/NewCircle.vue'
          ),
        meta: { authRequired: true, authorize: Role.FACILITATOR }
      },
      {
        path: 'circle/:id',
        name: 'edit-circle',
        component: () =>
          import(
            /* webpackChunkName: "client-chunk-login" */ '@/components/Facilitator/Circles/EditCircle.vue'
          ),
        meta: { authRequired: true, authorize: Role.FACILITATOR }
      },
      {
        path: 'profile',
        name: 'facilitator-account',
        component: () =>
          import(
            /* webpackChunkName: "client-chunk-login" */ '@/components/Facilitator/Profile/index.vue'
          ),
        meta: { authRequired: true, authorize: Role.FACILITATOR }
      },
      {
        path: 'notifications',
        name: 'facilitator-notifications',
        component: () =>
          import(
            /* webpackChunkName: "client-chunk-login" */ '@/components/Facilitator/Notifications/index.vue'
          ),
        meta: { authRequired: true, authorize: Role.FACILITATOR }
      },
      {
        path: 'integration',
        name: 'facilitator-integration',
        component: () =>
          import(
            /* webpackChunkName: "client-chunk-login" */ '@/components/Facilitator/Integrations/index.vue'
          ),
        meta: { authRequired: true, authorize: Role.FACILITATOR }
      }
    ]
  },
  {
    path: '/join',
    name: 'join',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/components/Facilitator/Onboarding'
      ),
    meta: { authRequired: true, authorize: Role.FACILITATOR }
  }
]
