import { Role } from '@/constants'

export default [
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/components/User/Dashboard.vue'
      ),
    meta: { authRequired: true, authorize: Role.USER }
  },
  {
    path: '/my-circles',
    name: 'my-circles',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/components/User/BookedCircle.vue'
      ),
    meta: { authRequired: true, authorize: Role.USER }
  },
  {
    path: '/account',
    name: 'user-account',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/components/User/Account/index.vue'
      ),
    meta: { authRequired: true, authorize: Role.USER }
  },
  {
    path: '/notifications',
    name: 'user-notifications',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/components/User/Notifications/index.vue'
      ),
    meta: { authRequired: true, authorize: Role.USER }
  },
  {
    path: '/circle/:id',
    name: 'user-circle',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/components/User/Group/index.vue'
      ),
    meta: { authRequired: true, authorize: Role.USER }
  }
  // {
  //   path: '/circle-video/:id',
  //   name: 'user-video',
  //   component: () =>
  //     import(
  //       /* webpackChunkName: "client-chunk-login" */ '@/components/Circle/Video/index.vue'
  //     ),
  //   meta: { authRequired: true, authorize: Role.USER }
  // }
]
