import Vue from 'vue'
import Router from 'vue-router'
import Head from 'vue-head'
import Home from '@/views/InnerPages/Home'
import CheckLogin from '@/views/Auth/CheckLogin'
import { Role } from '@/constants'

import { isNil } from 'lodash'
import store from '@/store'
import FacilitatorRoutes from './facilitator'
import UserRoutes from './user'

Vue.use(Router)
Vue.use(Head, { complement: process.env.VUE_APP_TITLE })

const baseRoutes = [
  { path: '/', name: 'home', component: Home, meta: { authRequired: false } },
  {
    path: '/check-login',
    name: 'check-login',
    component: CheckLogin,
    meta: { authRequired: false }
  },
  {
    path: '/about',
    name: 'about',
    component: () =>
      import(/* webpackChunkName: "client-chunk-login" */ '@/views/InnerPages/About.vue'),
    meta: { authRequired: false }
  },
  {
    path: '/contact',
    name: 'contact',
    component: () =>
      import(/* webpackChunkName: "client-chunk-login" */ '@/views/InnerPages/Contact.vue'),
    meta: { authRequired: false }
  },
  {
    path: '/terms',
    name: 'terms',
    component: () =>
      import(/* webpackChunkName: "client-chunk-login" */ '@/views/InnerPages/Terms.vue'),
    meta: { authRequired: false }
  },
  {
    path: '/privacy',
    name: 'privacy',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/views/InnerPages/Privacy.vue'
      ),
    meta: { authRequired: false }
  },
  {
    path: '/faq',
    name: 'faq',
    component: () =>
      import(/* webpackChunkName: "client-chunk-login" */ '@/views/InnerPages/FAQ.vue'),
    meta: { authRequired: false }
  },
  {
    path: '/sign-up',
    name: 'sign-up',
    component: () =>
      import(/* webpackChunkName: "client-chunk-login" */ '@/views/Auth/SignUp.vue'),
    meta: { authRequired: false }
  },
  {
    path: '/login',
    name: 'login',
    component: () =>
      import(/* webpackChunkName: "client-chunk-login" */ '@/views/Auth/Login.vue'),
    meta: { authRequired: false }
  },
  {
    path: '/forgot-password',
    name: 'forgot-password',
    component: () =>
      import(/* webpackChunkName: "client-chunk-login" */ '@/views/Auth/Forgot.vue'),
    meta: { authRequired: false }
  },
  {
    path: '/join-us',
    name: 'facilitators-join',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/views/InnerPages/FacilitatorsJoin.vue'
      ),
    meta: { authRequired: false }
  },
  {
    path: '/facilitators',
    name: 'facilitators',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/views/BookFacilitator.vue'
      ),
    meta: { authRequired: false }
  },
  {
    path: '/facilitators/:name/:id',
    name: 'facilitator-profile',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/components/Facilitator/FacilitatorProfile.vue'
      ),
    meta: { authRequired: false }
  },
  {
    path: '/circle-details/:id',
    name: 'circle-details',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/views/Circle/CircleDetails.vue'
      ),
    meta: { authRequired: false }
  },
  {
    path: '/quiz',
    name: 'quiz',
    component: () =>
      import(/* webpackChunkName: "client-chunk-login" */ '@/views/Quiz.vue'),
    meta: { authRequired: false }
  },
  {
    path: '/circles',
    name: 'circles',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/views/Circle/Circles.vue'
      ),
    meta: { authRequired: false }
  },
  {
    path: '/book/:slug/:id',
    name: 'book-circle',
    component: () =>
      import(
        /* webpackChunkName: "client-chunk-login" */ '@/views/Circle/Book.vue'
      ),
    meta: { authRequired: true }
  },
  { path: '*', redirect: '/' },

]

const routes = baseRoutes.concat(UserRoutes, FacilitatorRoutes)
const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

/**
 * Handle user redirections
 */
// eslint-disable-next-line consistent-return
router.beforeEach((to, from, next) => {
  const { authRequired, authorize } = to.meta
  const { user } = store.state.authentication
  next()

  if (authRequired && isNil(user)) {
    const q = new URLSearchParams(to.query).toString()
    const path = user === null ? '/login' : '/check-login'
    return next(`${path}?redirectUrl=${to.path}&${q}`)
  }

  if (
    authRequired &&
    user.role === Role.USER &&
    authorize === Role.FACILITATOR
  ) {
    next('/dashboard')
  }

  if (
    authRequired &&
    user.role === Role.FACILITATOR &&
    authorize === Role.USER
  ) {
    next('/facilitator')
  }

  next()
})

export default router
