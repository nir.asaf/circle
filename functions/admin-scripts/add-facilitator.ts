if (!process.argv[2] || !process.argv[3]) {
    console.log('Missing arguments!');
    console.log(
      'Example: node admin-scripts/add-admin.ts PROJECT_NAME EMAIL',
    );
    process.exit(1);
  }
  
  const project = process.argv[2];
  const email = process.argv[3];
  const admin = require('firebase-admin');
  
  const serviceAccount = require(`../../.secret/${project}.serviceAccountKey.json`);
  
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: `https://${serviceAccount.project_id}.firebaseio.com`,
  });

  async function setFacilitator() {
    const firebaseUser = await admin.auth().getUserByEmail(email);
    
    const db = admin.firestore();
    const docRef = db.collection('users').doc(firebaseUser.uid);
    const user = await docRef.set({
      role: 'facilitator'
    }, { merge: true })
    
    return admin.auth().setCustomUserClaims(firebaseUser.uid, {
      facilitator: true,
    });
  }
  
  setFacilitator();