if (!process.argv[2] || !process.argv[3]) {
    console.log('Missing arguments!');
    console.log(
      'Example: node admin-scripts/add-admin.ts PROJECT_NAME EMAIL',
    );
    process.exit(1);
  }
  
  const project = process.argv[2];
  const email = process.argv[3];
  const admin = require('firebase-admin');
  const serviceAccount = require(`../../.secret/${project}.serviceAccountKey.json`);
  
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: `https://${serviceAccount.project_id}.firebaseio.com`,
  });

  async function setAdmin() {
    const user = await admin.auth().getUserByEmail(email);
    console.log(user);
    return admin.auth().setCustomUserClaims(user.uid, {
      admin: true,
    });
  }

  async function setFaciliter() {
    const user = await admin.auth().getUserByEmail(email);
    console.log(user);
    return admin.auth().setCustomUserClaims(user.uid, {
        faciliter: true,
    });
  }
  
  setAdmin();