import * as admin from 'firebase-admin';
import onUserCreatedHandler from './handlers/onUserCreated';
import onFacilitatorHandler from './handlers/onFacilitatorCreated';
import onCircleCreatedHandler from './handlers/onCircleCreated';
import bookCircleHandler from './handlers/bookCircle';
// import setStripeConnectHandler from './handlers/setStripeConnect';
import setZoomHandler from './handlers/setZoom';
// import createUserStripeTokenHandler from './handlers/createUserStripeToken';
import getFacilitatorProfileHandler from './handlers/getFacilitatorProfile';
import sendGroupInviteHandler from './handlers/sendGroupInvite';
import deleteCircleHandler from './handlers/deleteCircle';

admin.initializeApp();

export const onUserCreated = onUserCreatedHandler;
export const onFacilitator = onFacilitatorHandler;
export const bookCircle = bookCircleHandler;
// export const setStripeConnect = setStripeConnectHandler;
// export const createUserStripeToken = createUserStripeTokenHandler;
export const onCircleCreated = onCircleCreatedHandler;
export const getFacilitatorProfile = getFacilitatorProfileHandler;
export const setZoom = setZoomHandler;
export const sendGroupInvite =  sendGroupInviteHandler;
export const deleteCircle = deleteCircleHandler;
