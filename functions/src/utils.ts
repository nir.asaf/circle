import * as functions from 'firebase-functions';

// this wraps functions.https.onRequest with a global error handler
export function onRequest(
  handler: (
    request: functions.https.Request,
    response: functions.Response,
  ) => Promise<void>,
): functions.HttpsFunction {
  return functions.https.onRequest(async function(request, response) {
    try {
      await handler(request, response);
    } catch (e) {
      console.error(e);
      response.status(500).send({});
    }
  });
}

export function generatePassword() {
  return Math.random().toString(36).slice(-8);
}