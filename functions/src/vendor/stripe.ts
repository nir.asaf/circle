// import * as Stripe from 'stripe';
import * as functions from 'firebase-functions'
import { ChargeDetails, User } from '../schema'

const STRIPE_SECRET_KEY = functions.config().stripe.secret_key
const stripe = require('stripe')(STRIPE_SECRET_KEY)

export const deleteStripeUser = async (user: User) =>
  await stripe.customers.del(user.stripeId)

export const createNewStripeCustomer = async (email: string, token: string) =>
  await stripe.customers.create({ email: email, source: token })

export const createStripeCustomer = async (user: any) => {
  try {
    return await stripe.customers.create({
      email: user.email,
      name: `${user.firstName} ${user.lastName}`
    })
  } catch (err) {
    console.log(err)
    return err
  }
}

export const stripeConnectAccount = async (code: string) => {
  return await stripe.oauth.token({
    grant_type: 'authorization_code',
    code: code
  })
}

export const stripeRejectAccount = async (account: string) => {
  stripe.accounts.reject(account, { reason: 'other' })
}

// Charge with customer id
export const charge = async (chargeDetails: ChargeDetails) => {
  const {
    totalPrice,
    stripeCustomerId,
    discountedPrice,
    plan,
    email
  } = chargeDetails
  let price
  if (discountedPrice) {
    price = Math.round(Number(discountedPrice.toFixed(2)) * 100)
  } else {
    price = Math.round(Number(totalPrice.toFixed(2)) * 100)
  }

  try {
    return await stripe.charges.create({
      amount: price,
      currency: 'usd',
      customer: stripeCustomerId,
      description: `Circle - Plan #${plan} Email: ${email}`,
      metadata: { plan: plan },
      receipt_email: email
    })
  } catch (error) {
    throw new Error(error)
  }
}

// Charge with source
export const singleCharge = async (chargeDetails: ChargeDetails) => {
  const {
    totalPrice,
    discountedPrice,
    facilitator,
    token,
    email,
    plan
  } = chargeDetails
  const price =
    Math.round(Number(discountedPrice.toFixed(2)) * 100) ||
    Math.round(Number(totalPrice.toFixed(2)) * 100)

  if (facilitator.stripe_user_id) {
    return await stripe.paymentIntents.create({
      payment_method_types: ['card'],
      amount: price,
      currency: 'usd',
      application_fee_amount: price * 0.1,
      transfer_data: {
        destination: facilitator.stripe_user_id
      }
    })
  }
  return await stripe.charges.create({
    amount: price,
    currency: 'usd',
    source: token,
    description: `Circle - Plan #${plan} Email: ${email}`,
    metadata: { plan: plan },
    receipt_email: email
  })
}

export const subscriptionCharge = async (chargeDetails: ChargeDetails) => {
  const { user, facilitator } = chargeDetails
  if (facilitator.stripe_user_id) {
    // application_fee_amount: totalPrice * 0.1,
    console.log('With stripe id')
    return await stripe.subscriptions.create({
      customer: user.stripeCustomerId,
      items: [
        {
          price: chargeDetails.circle.priceId
        }
      ],
      expand: ['latest_invoice.payment_intent'],
      transfer_data: {
        destination: facilitator.stripe_user_id
      }
    })
  }
  console.log('Without stripe id')
  return await stripe.subscriptions.create({
    customer: user.stripeCustomerId,
    items: [
      {
        price: chargeDetails.circle.priceId
      }
    ]
  })
}

export const fetchingAccount = async (id: string) =>
  await stripe.accounts.retrieve({ stripe_account: id })

export const createProduct = async (name: string) => {
  try {
    const product = await stripe.products.create({ name: name })
    return product.id
  } catch (e) {
    console.log(e)
    return null
  }
}

export const createPrice = async (productId: string, frequencyType: string) => {
  try {
    const price = await stripe.prices.create({
      unit_amount: 1,
      currency: 'usd',
      recurring: { interval: frequencyType },
      product: productId
    })
    return price.id
  } catch (e) {
    console.log(e)
    return null
  }
}
