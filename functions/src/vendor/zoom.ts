import * as functions from 'firebase-functions'
import axios from 'axios';

const ZOOM_CLIENT_ID = functions.config().zoom.client_id
const ZOOM_SECRET_KEY = functions.config().zoom.secret_key

const zoomInstance = axios.create({
    baseURL: 'https://zoom.us/',
    headers: { Authorization: Buffer.from(`${ZOOM_CLIENT_ID}:${ZOOM_SECRET_KEY}`).toString('base64')},
});

console.log(zoomInstance)

export const getAccessToken = async (code: string) => {
    return axios.post(`/oauth/token?grant_type=authorization_code&code=${code}`)
}

export const refreshAccessToken = async (refresh_token: string) => {
    return axios.post(`/oauth/token?grant_type=refresh_token&refresh_token=${refresh_token}`)
}