import * as functions from 'firebase-functions';
import { Email } from '../schema';
import * as sgMail from '@sendgrid/mail';
const API_KEY = functions.config().sendgrid.key;
sgMail.setApiKey(API_KEY);

const TEMPLATE_ID: any = {
    USER_WELCOME_EMAIL: 'd-b5daf55d10c5427e9c0127717588d25f',
    FACILITATOR_USER_WELCOME_EMAIL: 'd-989fadc9a6da49c892d53e5ecfb534f0'
}

const FORM = 'love@holdcircle.com'

export const sendEmail = async(emailData: Email) => {
    const { subject = 'welcome' , email, firstName, templateName } = emailData

    const msg = {
        to: email,
        from: FORM,
        templateId: TEMPLATE_ID[templateName],
        dynamic_template_data: {
            subject: subject,
            name: firstName,
        },
    };

    return sgMail.send(msg);

}
