import * as admin from 'firebase-admin'

export interface User {
  created: admin.firestore.Timestamp
  email: string
  phone: string
  firstName: string
  lastName: string
  stripeId: string
}

export interface ChargeDetails {
  totalPrice: number
  discountedPrice: number
  token: string
  stripeCustomerId?: string
  plan: string
  email: string
  metadata?: any
  circle: any
  facilitator: any
  user: any
}

export interface Email {
  subject?: string
  email: string
  firstName: string
  templateName: string
}

export interface Circle {
  id: string
  facilitatorId: string
  title: string
  videoType: string
  videoURL: string
  imageUrl: string
  users: []
  frequency: { type: 'week' }
  dateItem: {},
  price: number
}

export interface Group {
  facilitatorId: string
  title: string
  videoType: string
  videoURL: string
  circleId: string
  usersIds: [],
  usersObjs: [],
}

export interface Conversation {
  facilitatorId: string
  title: string
  circleId: string
  groupId: string
  imageUrl: string
  dateItem: {},
  videoType: string,
  videoURL: string
}
