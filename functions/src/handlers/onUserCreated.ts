import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { sendEmail } from '../vendor/email';
import { EMAIL_TEMPLATE_NAME } from '../constants';
// const stripe = require('stripe')(functions.config().stripe.secret_key);

export default functions.firestore
  .document('users/{userId}')
  .onCreate(async (snap) => {
    const user = snap.data();
    if (!user) throw new Error('onUserCreated no user');
    try {
      //todo: move this back when ready for live

      // const customer = await stripe.customers.create({
      //   email: user.email,
      //   name: `${user.firstName} ${user.lastName}`,
      //   phone: user.phone,
      // });
      await snap.ref.set(
        {
          created: new Date(),
          stripeCustomerId: 'test' //customer.id todo: move this back when ready to go live
        },
        { merge: true },
      );
    } catch(err) {
      console.log(err)
    }

    const customClaims = {
      admin: user.role === 'admin',
      facilitator: user.role === 'facilitator',
      user: user.role === 'user',
    }

    await admin.auth().setCustomUserClaims(user.uid, customClaims)
    await sendEmail({subject: 'Welcome to circle', email: user.email, firstName: user.firstName, templateName: EMAIL_TEMPLATE_NAME.USER_WELCOME_EMAIL})
  });
