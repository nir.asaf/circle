import * as functions from 'firebase-functions'
import { addCircleToUser } from '../modal/circle'
import { read } from '../modal/generic'
// import { singleCharge, subscriptionCharge } from '../vendor/stripe'

export default functions.https.onCall(async (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError(
      'unauthenticated',
      'Authentication error'
    )
  }
  const userId = data.id
  const circleId = data.circleId
  const circle = await read(circleId, 'circles')
  if (!circle) {
    return 'Circle does not exist.'
  }
  circle.id = circleId
  const user = await read(userId, 'users')
  if (!user) {
    return 'User does not exist.'
  }
  user.id = userId
  if (!user.card || !user.card.token) {
    return 'User does not have a credit card inserted.'
  }

  // user.card.token

  // if(!facilitator.stripe_user_id){
  //   throw new functions.https.HttpsError('aborted', 'Facilitator does not have connection to his stripe account. Therefore he can not receive any payments.')
  // }

  //todo: move this back when ready for going live

  // const facilitator = await read(circle.facilitatorId, 'facilitators')

  // const chargeBody = {
  //   totalPrice: parseFloat(circle.price),
  //   token: 'tok_visa',
  //   discountedPrice: 0,
  //   plan: 'Book circle',
  //   email: user.email,
  //   circle: circle,
  //   facilitator: facilitator,
  //   user: user
  // }

  // if (!circle.recurringGroup) {
  //   await singleCharge(chargeBody)
  //     .then(() => {
  //       console.log('singleCharge')
  //     })
  //     .catch(err => {
  //       console.log(err)
  //       throw new functions.https.HttpsError('aborted', err.message)
  //     })
  // } else {
  //   await subscriptionCharge(chargeBody)
  //     .then(() => {
  //       console.log('subscriptionCharge')
  //     })
  //     .catch(err => {
  //       console.log(err)
  //       throw new functions.https.HttpsError('aborted', err.message)
  //     })
  // }

  console.log('beforeAddCircle')
  return await addCircleToUser(circle, user)
})
