import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import { read } from '../modal/generic'
import { generatePassword } from '../utils'

export default functions.https.onCall(async (data, context) => {
    if (!context.auth) {
        throw new functions.https.HttpsError('unauthenticated','Authentication error')
    }
    const { groupId, email, firstName } = data
    const facilitator = await read(context.auth.uid, 'facilitators')
    if (!facilitator.circles.includes(groupId)) {
        throw new functions.https.HttpsError('unauthenticated','Authorization error')
    }

    const groupRef = await admin.firestore().collection('groups').doc(groupId)
    
    let authUser = await admin.auth().getUserByEmail(email)
    if (!authUser) {
        authUser = await admin.auth().createUser({
            email: email,
            displayName: firstName,
            password: generatePassword()
        })
        const userData = {
            email,
            firstName,
        }
        await admin.firestore().collection('users').doc(authUser.uid).set(userData)
    }

    await admin.firestore().collection('users').doc(authUser.uid).update({status: 'pending'})
    await groupRef.collection('participants').doc(authUser.uid).set({
        email: email,
        displayName: firstName,
        status: 'pending'
    })
})