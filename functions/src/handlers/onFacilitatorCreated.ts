import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import { sendEmail } from '../vendor/email'
import { EMAIL_TEMPLATE_NAME } from '../constants'
// const stripe = require('stripe')(functions.config().stripe.secret_key)

export default functions.firestore
  .document('facilitators/{facilitatorId}')
  .onCreate(async snap => {
    const facilitator = snap.data()
    if (!facilitator) throw new Error('onFacilitatorCreated no user')

    // Creating a profile for this facilitators
    let profile = null
    try {
      const profileDate = {
        circles: facilitator.circles,
        createTimestamp: new Date(),
        firstName: facilitator.firstName,
        lastName: facilitator.lastName,
        uid: facilitator.uid,
        updateTimestamp: new Date(),
      }
      profile = await admin
        .firestore()
        .doc(`/facilitatorsProfile/${facilitator.uid}`)
        .set(profileDate)
    } catch (err) {
      console.log(err)
    }

    try {
      // todo: move this back when ready for live

      // const customer = await stripe.customers.create({
      //   email: facilitator.email,
      //   name: `${facilitator.firstName} ${facilitator.lastName}`,
      //   phone: facilitator.phone
      // })
      await snap.ref.set(
        {
          created: new Date(),
          stripeCustomerId: 'test', //customer.id, todo: move this back when ready for live
          profileCreated: !!profile
        },
        { merge: true }
      )
    } catch (err) {
      console.log(err)
    }

    const customClaims = {
      facilitator: facilitator.role === 'facilitator',
      role: 'facilitator'
    }
    await admin.auth().setCustomUserClaims(facilitator.uid, customClaims)
    await sendEmail({
      subject: 'Welcome to circle',
      email: facilitator.email,
      firstName: facilitator.firstName,
      templateName: EMAIL_TEMPLATE_NAME.FACILITATOR_USER_WELCOME_EMAIL
    })
  })
