import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { getAccessToken } from '../vendor/zoom';

export default functions.https.onCall(async (data, context) => {
    if (!context.auth) {
        throw new functions.https.HttpsError(
          'unauthenticated',
          'Authenticaiton error',
        );
    }
    const code = data.code
    const uid = context.auth.uid;

    try {
        const res = await getAccessToken(code)
        const userRef = admin
        .firestore()
        .collection('facilitators')
        .doc(uid);

        await userRef.update(res);
    } catch(e) {     
        console.error(e);
        throw new functions.https.HttpsError('aborted',
            e.message || 'failed to connect to zoom',
        );
    }
})