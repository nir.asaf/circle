import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { Circle } from '../schema';

export default functions.https.onCall(async (data, context) => {
    const facilitatorId = data.facilitatorId
    
    try {
        const facilitatorProfileRef = admin
        .firestore()
        .collection('facilitatorsProfile')
        .doc(facilitatorId)
    
        const facilitatorProfile = (await facilitatorProfileRef.get()).data()
        if (!facilitatorProfile) throw new functions.https.HttpsError('unknown', 'failed to get facilitator Profile');
        
        delete facilitatorProfile.phone
        delete facilitatorProfile.email
        delete facilitatorProfile.role

        const circles: Circle[] = []
        const circlesRef = await facilitatorProfileRef.collection('circles').get()
        circlesRef.forEach(circle => {
            circles.push(circle.data() as Circle)
        })
        return {...facilitatorProfile, circles}
    } catch(e) {     
        console.error(e);
        throw new functions.https.HttpsError('aborted',
            e.message || 'failed to get facilitator Profile',
        );
    }
})