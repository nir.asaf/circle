import * as functions from 'firebase-functions'
import { deleteCircle } from '../modal/circle'
import { read, readFromPath } from '../modal/generic'

export default functions.https.onCall(async (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError(
      'unauthenticated',
      'Authentication error'
    )
  }
  const circleId = data.circleId
  if (!circleId) {
    return 'CircleId must not be null'
  }
  const circle = await read(circleId, 'circles')
  if (!circle) {
    return 'Circle does not exist.'
  }
  circle.id = circleId
  console.log(circle.facilitatorId)
  console.log(circle.id)
  // const facilitator = await read(circle.facilitatorId, 'facilitators')
  const facilitatorCircle = await readFromPath(
    `facilitatorsProfile/${circle.facilitatorId}/circles/${circle.id}`
  )
  console.log(JSON.stringify(facilitatorCircle))

  if(facilitatorCircle){
    console.log('beforeDeleteCircle')
    return await deleteCircle(
      circle.id,
      circle.facilitatorId,
      facilitatorCircle.users
    )
  }
  return null
})
