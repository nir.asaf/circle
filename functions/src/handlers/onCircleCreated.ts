import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import { Circle, Group, Conversation } from '../schema'
import { createProduct, createPrice } from '../vendor/stripe'
import { read } from '../modal/generic'

// onWrite()
export default functions.firestore
  .document('circles/{circleId}')
  .onWrite(async (change: any, context: functions.EventContext) => {
    if (!change.after.exists) return
    const circleData = (await change.after.data()) as Circle
    // const videoURL = await (change.before.data()).videoURL
    console.log('videoURL')
    console.log(circleData.videoURL)
    console.log(JSON.stringify(circleData))
    if (!circleData) throw new Error('No data to update')
    circleData.users = []

    const groupData: Group = {
      title: circleData.title,
      videoType: circleData.videoType,
      videoURL: circleData.videoURL,
      facilitatorId: circleData.facilitatorId,
      circleId: context.params.circleId,
      usersIds: [],
      usersObjs: []
    }

    const conversationData: Conversation = {
      title: circleData.title,
      facilitatorId: circleData.facilitatorId,
      circleId: context.params.circleId,
      groupId: context.params.circleId,
      imageUrl: circleData.imageUrl,
      dateItem: circleData.dateItem,
      videoType: circleData.videoType,
      videoURL: circleData.videoURL
    }
    const oldDoc = await read(context.params.circleId, 'circles')
    if (!oldDoc) return
    console.log(oldDoc.circleId)
    let productId = oldDoc.productId
    let priceId = oldDoc.priceId
    if (!productId && !priceId) {
      productId = await createProduct(circleData.title)
      priceId = await createPrice(productId, circleData.frequency.type)
    }

    try {
      if (productId && priceId && !oldDoc.productId && !oldDoc.priceId) {
        await admin
          .firestore()
          .doc(`circles/${context.params.circleId}`)
          .update({
            id: context.params.circleId,
            productId: productId,
            priceId: priceId
          })
          .catch(err => {
            console.log(err)
          })
      }
      await admin
        .firestore()
        .doc(
          `/facilitatorsProfile/${circleData.facilitatorId}/circles/${circleData.id}`
        )
        .set(circleData)
        .catch(err => {
          console.log(err)
        })
      await admin
        .firestore()
        .doc(`/groups/${context.params.circleId}`)
        .set(groupData)
        .catch(err => {
          console.log(err)
        })
      await admin
        .firestore()
        .doc(`/conversations/${context.params.circleId}`)
        .set(conversationData)
        .catch(err => {
          console.log(err)
        })
      await admin
        .firestore()
        .doc(
          `/facilitatorsProfile/${circleData.facilitatorId}/circles/undefined`
        )
        .delete()
        .catch(err => {
          console.log(err)
        })
    } catch (err) {
      console.log(err)
    }
  })
