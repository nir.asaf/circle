import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'

export default functions.https.onCall(async (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError(
      'unauthenticated',
      'Authentication error'
    )
  }

  return admin
    .firestore()
    .collection(data.path)
    .doc(context.auth.uid)
    .update({
      card: data.card
    })
})
