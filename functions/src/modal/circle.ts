import * as admin from 'firebase-admin'

export const addCircleToUser = async (circle: any, user: any) => {
  const userId = user.id
  const userObj = {
    id: user.id,
    firstName: user.firstName,
    lastName: user.lastName,
    imageUrl: user.imageUrl ?? null
  }
  console.log('userObj')
  console.log(userObj)
  let facilitatorCircle: FirebaseFirestore.DocumentData | undefined = {
    users: []
  }
  await admin
    .firestore()
    .doc(`facilitatorsProfile/${circle.facilitatorId}/circles/${circle.id}`)
    .get()
    .then(ref => {
      facilitatorCircle = ref.data()
    })
  if (
    facilitatorCircle.users &&
    circle.numOfParticipants < facilitatorCircle.users.length + 1
  ) {
    console.log('All the places are already booked.')
    return { status: 0, message: 'All the places are already booked.' }
  }
  let res = null
  if (!userId) return { status: 0, message: 'UserId is null.' }
  try {
    await admin
      .firestore()
      .collection('groups')
      .doc(circle.id)
      .update({
        usersIds: admin.firestore.FieldValue.arrayUnion(userId),
        usersObjs: admin.firestore.FieldValue.arrayUnion(userObj)
      })
      .catch(err => {
        console.log(err)
      })
    await admin
      .firestore()
      .collection('facilitatorsProfile')
      .doc(circle.facilitatorId)
      .collection('circles')
      .doc(circle.id)
      .update({
        users: admin.firestore.FieldValue.arrayUnion(userId)
      })
      .catch(err => {
        console.log(err)
      })
    // await admin
    //   .firestore()
    //   .collection('circles')
    //   .doc(circle.id)
    //   .update({
    //     'facilitator.users': admin.firestore.FieldValue.arrayUnion(userId)
    //   })
    //   .catch(err => {
    //     console.log(err)
    //   })

    const price = parseInt(circle.price)
    const forFacilitator = price - price * 0.1
    await admin
      .firestore()
      .collection('facilitators')
      .doc(circle.facilitatorId)
      .update({
        participants: admin.firestore.FieldValue.increment(1),
        earnings: admin.firestore.FieldValue.increment(forFacilitator)
      })
      .catch(err => {
        console.log(err)
      })
    res = await admin
      .firestore()
      .collection('users')
      .doc(userId)
      .update({
        circles: admin.firestore.FieldValue.arrayUnion(circle.id)
      })
      .catch(err => {
        console.log(err)
      })
  } catch (e) {
    console.error(e)
    return e
  }
  return { status: 1, data: res }
}

export const deleteCircle = async (
  circleId: any,
  facilitatorId: any,
  users: any
) => {
  let deleted = true

 const res = await admin
   .firestore()
   .doc(`circles/${circleId}`)
   .delete()
   .catch(err => {
     console.log(err)
     deleted = false
   })
  await admin
    .firestore()
    .doc(`facilitatorsProfile/${facilitatorId}/circles/${circleId}`)
    .delete()
    .catch(err => {
      console.log(err)
      deleted = false
    })
  await admin
    .firestore()
    .collection('users')
    .where('id', 'in', users)
    .get()
    .then(res => {
      res.docs.forEach(async user => {
        await admin
          .firestore()
          .doc(`users/${user.id}`)
          .update({
            circles: admin.firestore.FieldValue.arrayRemove(circleId)
          })
          .catch(err => {
            console.log(err)
            deleted = false
          })
      })
    })
    .catch(err => {
      console.log(err)
    })
  return { status: deleted, data: res }
}
