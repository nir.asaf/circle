import * as admin from 'firebase-admin'

export const read = async (userId: any, path: any) => {
  let result = null
  try {
    result = await admin
      .firestore()
      .collection(path)
      .doc(userId)
      .get()
  } catch (e) {
    console.error(e)
    return e
  }
  // @ts-ignore
  return result && result.exists ? result.data() : null
}
export const readFromPath = async (docPath: any) => {
  let result = null
  try {
    result = await admin
      .firestore()
      .doc(docPath)
      .get()
  } catch (e) {
    console.error(e)
    return e
  }
  // @ts-ignore
  return result && result.exists ? result.data() : null
}

